# **How to use (●'◡'●)**

<br><br>
**Basic setup:**
<pre><code>// Composer stuff
require_once 'vendor/autoload.php';
use Discord\Webhook;
</code></pre>

<br><br>

**Setup discord webhooks:**
<pre>
$url = [
    'https://canary.discordapp.com/api/v7/webhooks/1',
    'https://canary.discordapp.com/api/v7/webhooks/2'
];

$wh = new Webhook($url);
</pre>
_or_
<pre>
$wh = new Webhook('https://canary.discordapp.com/api/v7/webhooks/1');
</pre>

<br><br>
**More ^_^**<br>
`$wh->setUsername('Yupdab, the discord webhook');` Set username
<br>
`$wh->setAvatar('https://imagetourl.com/avatar.png');` Set avatar
<br>
`$wh->setThumbnail('https://imagetourl.com/thumbnail.png');` Set thumbnail
<br>
`$wh->setContent('NEW MESSAGE');` Set content
<br>

There are two ways to setup the embed-color:
<pre>
// Set custom color value
$wh->setColor('12745742');
</pre>
<pre>
// Use color from class (Webhook.php -> line 11)
$wh->setColor(self::COLOR_DARK_BLUE);
</pre>
<br>
Set webhook field (max 2):
<pre>
$arr = [
    'name'   => 'Field 1',
    'value'  => 'Wohoo',
    'inline' => true
];
$wh->setField($arr);
$arr2 = [
    'name'   => 'Field 2',
    'value'  => 'Test value',
    'inline' => true
];
$wh->setField2($arr2);
</pre>

Aaaand, last but not least: Send it!<br>
`$wh->send()`
