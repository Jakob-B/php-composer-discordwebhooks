<?php
declare(strict_types=1);

namespace Discord;

class Webhook
{
    /**
     * Embed colors
     */
    const COLOR_BLACK       = 0;
    const COLOR_AQUA        = 1752220;
    const COLOR_GREEN       = 3066993;
    const COLOR_BLUE        = 3447003;
    const COLOR_PURPLE      = 10181046;
    const COLOR_GOLD        = 15844367;
    const COLOR_ORANGE      = 15105570;
    const COLOR_RED         = 15158332;
    const COLOR_GREY        = 9807270;
    const COLOR_DARKER_GREY = 8359053;
    const COLOR_NAVY        = 3426654;
    const COLOR_DARK_AQUA   = 1146986;
    const COLOR_DARK_GREEN  = 2067276;
    const COLOR_DARK_BLUE   = 2123412;
    const COLOR_DARK_PURPLE = 7419530;
    const COLOR_DARK_GOLD   = 12745742;
    const COLOR_DARK_ORANGE = 11027200;
    const COLOR_DARK_RED    = 10038562;
    const COLOR_DARK_GREY   = 9936031;
    const COLOR_LIGHT_GREY  = 12370112;
    const COLOR_DARK_NAVY   = 2899536;

    /**
     * @var string
     */
    private $color;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @var string
     */
    private $username;

    private $field;

    /**
     * @var string|array
     */
    private $url;

    /**
     * @var string
     */
    private $thumbnail;

    /**
     * @var bool
     */
    private $errorReport;

    /**
     * @var array
     */
    private $field2;

    /**
     * @var string
     */
    private $content;

    public function __construct($url = null, bool $errorReport = false)
    {
        $this->url = $url;
        $this->errorReport = $errorReport;
    }

    /**
     * @param int $color
     * Set color of embed
     */
    public function setColor(int $color)
    {
        $this->color = $color;
    }

    /**
     * @param string $content
     * Set content
     */
    public function setContent(string $content) {
        $this->content = $content;
    }

    /**
     * @param string $avatar
     * Set avatar for bot
     */
    public function setAvatar(string $avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @param string $username
     * Set bot-username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    /**
     * @param string $thumbnail
     * Sets thumbnail
     */
    public function setThumbnail(string $thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @param array|null $fieldData
     * Sets field 1
     */
    public function setField(array $fieldData = null)
    {
        $this->field = [
            'name'   => $fieldData['name'],
            'value'  => $fieldData['value'],
            'inline' => $fieldData['inline'],
        ];
    }

    /**
     * @param array|null $fieldData
     * Sets field 2
     */
    public function setField2(array $fieldData = null)
    {
        $this->field2 = [
            'name'   => $fieldData['name'],
            'value'  => $fieldData['value'],
            'inline' => $fieldData['inline'],
        ];
    }

    public function send()
    {
        $result = '';
        foreach ($this->url as $url) {
            if (is_array($this->field) && is_array($this->field2)) {
                $result .= $this->send2($url);
            }

            $result .= $this->send1($url);
        }
        return $result;
    }

    public function send2($url)
    {
        $hookObject = json_encode([
            'content'    => $this->content ?? '',
            'username'   => $this->username ?? 'Anonymous',
            'avatar_url' => $this->avatar ?? 'https://stylizedbay.com/wp-content/uploads/2018/02/unknown-avatar.jpg',
            'tts'        => false,
            'embeds'     => [
                [
                    'type' => 'rich',

                    'description' => '',

                    'timestamp' => $this->timestamp ?? '',

                    'color' => $this->color ?? self::COLOR_LIGHT_GREY,

                    'thumbnail' => [
                        'url' => $this->thumbnail ?? ''
                    ],

                    'fields' => [
                        [
                            'name'   => $this->field['name'] ?? ' ',
                            'value'  => $this->field['value'] ?? ' ',
                            'inline' => $this->field['inline'] ?? true,
                        ],
                        [
                            'name'   => $this->field2['name'] ?? ' ',
                            'value'  => $this->field2['value'] ?? ' ',
                            'inline' => $this->field2['inline'] ?? true,
                        ]

                    ]
                ]
            ]

        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $ch = $this->sendReal($hookObject, $url);

        return curl_exec($ch);
    }

    public function send1($url)
    {
        $hookObject = json_encode([
            'content'    => $this->content ?? '',
            'username'   => $this->username ?? 'Anonymous',
            'avatar_url' => $this->avatar ?? 'https://stylizedbay.com/wp-content/uploads/2018/02/unknown-avatar.jpg',
            'tts'        => false,
            'embeds'     => [
                [
                    'type' => 'rich',

                    'description' => '',

                    'timestamp' => $this->timestamp ?? '',

                    'color' => $this->color ?? self::COLOR_LIGHT_GREY,

                    'thumbnail' => [
                        'url' => $this->thumbnail ?? ''
                    ],

                    'fields' => [
                        [
                            'name'   => $this->field['name'] ?? ' ',
                            'value'  => $this->field['value'] ?? ' ',
                            'inline' => $this->field['inline'] ?? true,
                        ]

                    ]
                ]
            ]

        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $ch = $this->sendReal($hookObject, $url);

        return curl_exec($ch);
    }

    private function sendReal($hookObject, $url = null)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-type: application/json']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $hookObject);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        return $ch;
    }
}